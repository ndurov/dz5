var studentsAndPoints = [
		'Алексей Петров', 0,
		'Ирина Овчинникова', 60,
		'Глеб Стукалов', 30,
		'Антон Павлович', 30,
		'Виктория Заровская', 30,
		'Алексей Левенец', 70,
		'Тимур Вамуш', 30,
		'Евгений Прочан', 60,
		'Александр Малов', 0
	],
	studentMaxPoints,
	maxPoints;

console.log('Список студентов:');
for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
	if (maxPoints === undefined || studentsAndPoints[i + 1] > maxPoints ) {
		studentMaxPoints = studentsAndPoints[i];
		maxPoints        = studentsAndPoints[i + 1];
	}
}

console.log('\nСтудент набравший максимальный балл:');
console.log('Студент %s имеет максимальный бал %d', studentMaxPoints, maxPoints);

studentsAndPoints.push('Николай Фролов', 0);
studentsAndPoints.push('Олег Боровой', 0);

if (studentsAndPoints.indexOf('Антон Павлович') != -1) {
	studentsAndPoints[studentsAndPoints.indexOf('Антон Павлович') + 1] += 10;
}
if (studentsAndPoints.indexOf('Николай Фролов') != -1) {
	studentsAndPoints[studentsAndPoints.indexOf('Николай Фролов') + 1] += 10;
}

console.log('\nСтуденты не набравшие баллов:');
for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	if (studentsAndPoints[i + 1] == 0) {
		console.log(studentsAndPoints[i]);
		studentsAndPoints.splice(i, 2);
		i -= 2;
		imax -= 2;
	}
}

console.log('\nСтуденты набравшие баллы:');
for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log('Студент %s набрал %d баллов', studentsAndPoints[i], studentsAndPoints[i + 1]);
}